package cl.salemlabs.androidfirebase;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

import static java.security.AccessController.getContext;

public class LoginActivity extends BaseActivity {
    private static final String TAG = "Login";
    private FirebaseAuth mAuth;

    private Button btnEntrar;
    private LoginButton btnFacebook;
    private EditText txtEmail, txtClave;

    private Context context;
    private CallbackManager callbackManager;

    private FirebaseAuth.AuthStateListener mAuthListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        if (mAuth != null && mAuth.getCurrentUser() != null) {
            cambiarVentana(MenuPrincipalActivity.class, true);
        } else {

            callbackManager = CallbackManager.Factory.create();

            context = this;

            txtClave = (EditText) findViewById(R.id.txtClave);
            txtEmail = (EditText) findViewById(R.id.txtEmail);

            btnEntrar = (Button) findViewById(R.id.btnEntrar);

            btnEntrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    validarUsuario(txtEmail.getText().toString(), txtClave.getText().toString());
                }
            });

            btnFacebook = (LoginButton) findViewById(R.id.btnFacebook);
            List<String> permisos = new ArrayList<>();
            permisos.add("public_profile");
            permisos.add("email");
            btnFacebook.setReadPermissions(permisos);

            btnFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    showProgressDialog();
                    Log.d(TAG, "facebook:onSuccess:" + loginResult);
                    handleFacebookAccessToken(loginResult.getAccessToken());
                }

                @Override
                public void onCancel() {
                    Log.d(TAG, "facebook:onCancel");
                }

                @Override
                public void onError(FacebookException error) {
                    Log.d(TAG, "facebook:onError", error);
                }
            });
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        hideProgressDialog();
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(LoginActivity.this, "Autentificacion fallida.",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            cambiarVentana(MenuPrincipalActivity.class, true);
                        }
                    }
                });
    }

    private void validarUsuario(final String email, final String clave) {
        if (email.length() > 0 && clave.length() > 5) {
            if (Utils.isValidMail(email)) {

                showProgressDialog();
                mAuth.signInWithEmailAndPassword(email, clave)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                Log.d(TAG, "signIn:onComplete:" + task.isSuccessful());
                                hideProgressDialog();

                                if (task.isSuccessful()) {
                                    cambiarVentana(MenuPrincipalActivity.class);

                                } else {
                                    Utils.alert(context, R.string.app_name, R.string.error_autentificacion, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                                }
                            }
                        });
            } else {
                txtEmail.setError("Email no valido");
                txtEmail.requestFocus();
            }
        } else {
            Toast.makeText(context, "Debe ingresar Email y Clave", Toast.LENGTH_SHORT).show();
        }

    }
}
