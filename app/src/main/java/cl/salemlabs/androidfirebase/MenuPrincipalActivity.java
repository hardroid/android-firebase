package cl.salemlabs.androidfirebase;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;

import java.util.HashMap;
import java.util.Map;

import cl.salemlabs.androidfirebase.entidades.Comentario;

public class MenuPrincipalActivity extends BaseActivity {

    private static final String TAG = "ListadoComentarios";
    private RecyclerView rvComentarios;
    private FloatingActionButton btnComentarioNuevo;
    private FirebaseRecyclerAdapter<Comentario, ComentariosViewHolder> mAdapter;
    private DatabaseReference mDatabase;
    private static final String UID_LISTADO = "listadoComentarios";
    private static final String UID_LISTADO_DETALLE_LIKE = "listadoComentarios";
    private LinearLayoutManager mManager;
    private Context context;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);
        context = this;
        rvComentarios = (RecyclerView)findViewById(R.id.rvComentarios);
        btnComentarioNuevo = (FloatingActionButton)findViewById(R.id.btnComentarioNuevo);
        //Hacemos una referencia a la base de datos de firebase
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mAuth = FirebaseAuth.getInstance();


        //Iniciamos nuestro manager para el RecyclerView
        mManager = new LinearLayoutManager(this);
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        rvComentarios.setLayoutManager(mManager);
        //Acá consultamos el json con los comentarios por medio del UID
        Query postsQuery = getQuery(mDatabase);

        mAdapter = new FirebaseRecyclerAdapter<Comentario, ComentariosViewHolder>(Comentario.class,
                R.layout.comentario_item, ComentariosViewHolder.class, postsQuery) {
            @Override
            protected void populateViewHolder(ComentariosViewHolder viewHolder,final Comentario model, int position) {

                try {
                    final DatabaseReference postRef = getRef(position);

                    // Determine if the current user has liked this post and set UI accordingly
                    if (model.getDetalleLikes().containsKey(getUid())) {
                        viewHolder.imgLike.setImageResource(R.drawable.ic_toggle_star_24);
                    } else {
                        viewHolder.imgLike.setImageResource(R.drawable.ic_toggle_star_outline_24);
                    }

                    // Bind Post to ViewHolder, setting OnClickListener for the star button
                    viewHolder.enviarComentario(model, context, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Launch PostDetailActivity

                        }
                    }, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            DatabaseReference userPostRef = mDatabase.child(UID_LISTADO).child(model.getUid());//.child(UID_LISTADO_DETALLE_LIKE).child(postRef.getKey());
                            onStarClicked(userPostRef);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        rvComentarios.setAdapter(mAdapter);

        btnComentarioNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String usuario = (mAuth.getCurrentUser().getDisplayName()==null?mAuth.getCurrentUser().getEmail():mAuth.getCurrentUser().getDisplayName());

                NuevoComentario.getInstance().alertComentario(context, usuario, new NuevoComentario.OnClickComentarListener() {
                    @Override
                    public void onClickComentar(String tutulo, String comentario) {
                        enviarComentarioFirebase(tutulo,comentario);
                    }
                });
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.cerrar_sesion) {
            cerrarSesion();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void cerrarSesion() {
        mAuth.signOut();
        LoginManager.getInstance().logOut();
        cambiarVentana(LoginActivity.class, true);
    }

    private void onStarClicked(DatabaseReference postRef) {
        postRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Comentario p = mutableData.getValue(Comentario.class);
                if (p == null) {
                    return Transaction.success(mutableData);
                }

                if (p.getDetalleLikes().containsKey(getUid())) {
                    // Unstar the post and remove self from stars
                    p.setLikes((Integer.parseInt(p.getLikes()) - 1) + "");
                    p.getDetalleLikes().remove(getUid());
                } else {
                    // Star the post and add self to stars
                    p.setLikes((Integer.parseInt((p.getLikes().length()==0?"0":p.getLikes())) + 1) + "");
                    p.getDetalleLikes().put(getUid(), "1");
                }

                // Set value and report transaction success
                mutableData.setValue(p);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {
                // Transaction completed
                Log.d(TAG, "postTransaction:onComplete:" + databaseError);
            }
        });
    }

    private void enviarComentarioFirebase(String titulo, String s) {
        try {
            String usuario = mAuth.getCurrentUser().getEmail();

            String urlAvatar = (mAuth.getCurrentUser().getPhotoUrl()!=null?mAuth.getCurrentUser().getPhotoUrl().toString():"");
            final Comentario nuevoComentario = new Comentario();
            nuevoComentario.setTitulo(titulo);
            nuevoComentario.setMensaje(s);
            nuevoComentario.setUrlPerfil(urlAvatar);
            nuevoComentario.setUsuario(usuario);
            nuevoComentario.setLikes("0");
            HashMap<String, String> votacion = new HashMap<>();
            nuevoComentario.setDetalleLikes(votacion);



            DatabaseReference ref = mDatabase.child(UID_LISTADO);
            String key = ref.push().getKey();
            nuevoComentario.setUid(key);
            Map<String, Object> comentarioHash = nuevoComentario.toMap();
            ref.child(key).updateChildren(comentarioHash);
            //loadComentarios(this);

        } catch (Exception e) {
            Log.e(TAG, "Error obteniendo UID de Firebase " + e.getMessage());
        }
    }

    private Query getQuery(DatabaseReference mDatabase) {
        return mDatabase.child(UID_LISTADO);
    }

    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }
}
