package cl.salemlabs.androidfirebase;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by harttynarce on 03-10-16.
 */

public class BaseActivity extends AppCompatActivity {

    private ProgressDialog mProgressDialog;

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Loading...");
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public void cambiarVentana(Class<?> newActivity, boolean isFinish) {
        Intent i = new Intent(this, newActivity);
        startActivity(i);
        if (isFinish) {
            finish();
        }
    }

    public void cambiarVentana(Class<?> newActivity) {
        cambiarVentana(newActivity, false);
    }
}
