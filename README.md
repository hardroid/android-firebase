# Android + Firebase, por Harttyn Arce

Esta proyecto se desarrolló para ser mostrado en el Meetup organizado por Android GDG Santiago, para mas detalles
 [Ver Link](http://www.meetup.com/es/gdg-android-santiago/events/234096603/).

Pretendo mostrar un ejemplo práctico de las cosas que se pueden lograr utilizando Android
y Firebase como backend de Servicios

Dentro de este ejemplo se implementa la Autentificación con Firebase utilizando un correo y contraseña y también se integra
la autentificación utilizando Facebook.

Además de la autentificación, se implementa el uso de la base de datos en tiempo real que proporciona Firebase, permitiendo así 
ver en un ejemplo práctico de cómo se comporta dicha base de datos.

Link con APK de ejemplo [Descargar APK](https://www.dropbox.com/s/xlfzf1easte7h59/Android%2BFirebase.GDG.Santiago.apk?dl=0)
Link con PPT [Descargar Presentación](https://www.dropbox.com/s/r2zax2vlh6jbigq/Android%2BFirebase.pptx?dl=0)

## Android + Firebase

El proyecto se desarrolló en una rama maestra, utilizando unos cuantos commit al momento de terminar algunas funcionalidades.

## Introducción para replicar este proyecto

* Lo primero que se debe tener es una cuenta de Google, con esta se podrá acceder a la consola de Firebase en el siguiente link
[Consola de Firebase](https://firebase.google.com/?hl=es).

* Una vez ingresado a la consola de Firebase, se deben crear un proyecto y seguir los pasos para obtener el archivo google-services.json

* Ya con nuestro proyecto creado en Firebase, debemos crear un proyecto en Facebook siguiendo los pasos descritos en el siguiente 
 link [Crear proyecto Facebook](https://code.tutsplus.com/es/tutorials/quick-tip-add-facebook-login-to-your-android-app--cms-23837).

* Importante es obtener el app_id de facebook, junto con la clave secreta del proyecto [ver](https://cms-assets.tutsplus.com/uploads/users/362/posts/23837/image/myappid.png)
  
    
* Cuando agregues la plataforma Android, te pedirá los siguientes datos
  * Nombre del paquete de Google Play (cl.salemlabs.androidfirebase)
  * Nombre de la clase (cl.salemlabs.androidfirebase.LoginActivity)
  * Hashes de clave, este hash lo obtienes ejecutando el siguiente comando
         `keytool -exportcert -alias androiddebugkey -keystore ~/.android/debug.keystore | openssl sha1 -binary | openssl base64'
         Les pedirá una clave que es "android" (sin las comillas).
  * Por último, recuerda habilitar tu proyecto para que pueda ser utilizado en distintas 
    cuentas de facebook, para esto debes ir a la opción "Revisión de la Aplicación" 
    y habilitar la pregunta 
    ¿Quieres que AndroidFirebase sea una aplicación pública? 
    Este error al menos a mí se me olvido habilitar y cuando lo probé con otra 
    cuenta de facebook me mostro un mensaje no muy
    agradable :)

## Configurar Firebase para Autentificación

Ya con el proyecto de Firebase y Facebook ya podemos configurar la autentificación desde la consola de Firebase

* Entrar a la opción Auth.
* Entrar a la opción "Métodos de inicio de Sesión".
* Habilitar la opción de "Correo electrónico/contraseña".
* Habilitar la opción de "Facebook" pasando el App Id y la contraseña que obtuvimos al momento de crear el proyecto.
* Es importante pasar correctamente el App ID de Facebook y la contraseña correspondientes, ya que si no lo hacen no podrán iniciar sesión correctamente.

## Clonar Proyecto Android + Firebase

Ya con nuestros proyectos Firebase y Facebook configurados, podemos clonar el proyecto y cambiar las configuraciones necesarias para que funcione el ejemplo de este repositorio.

Para esto debemos realizar los siguientes pasos:
    
* Entrar al terminal de Git y ejecutar el siguiente comando dentro de la ruta que queramos
  "git clone https://hardroid@bitbucket.org/hardroid/android-firebase.git"
* Entrar al archivo res/values/strings.xml y cambiar el valor del tag 
  <string name="facebook_app_id">323236178044057</string>
  por el app_id de facebook que ustedes crearon.
* Reemplazar el archivo app/google-services.json por el archivo que ustedes 
  descargaron al momento de crear el proyecto en Firebase.
       


Si hiciste todos los pasos mencionados anteriormente deberías poder ejecutar el proyecto de forma correcta.

Para poder probar el acceso con correo y contraseña de firebase debes crear un usuario y contraseña desde la opción de 
Auth/AÑADIR USUARIO en la consola de Firebase.


Espero que este ejemplo sea de ayuda para todos los desarrolladores que lo necesiten, cualquier duda, acotación o sugerencia 
no duden en contactarme a mi correo personal harttyn.arce@gmail.com

Harttyn A. Arce
[LinkedIn](https://cl.linkedin.com/in/harttyn-arce-405bb535)

