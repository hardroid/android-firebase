package cl.salemlabs.androidfirebase;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import cl.salemlabs.androidfirebase.entidades.Comentario;


public class ComentariosViewHolder extends RecyclerView.ViewHolder {

    public TextView txtUsuario;
    public TextView txtComentario;
    public TextView txtLike;
    public TextView txtTitulo;
    public ImageView imgAvatar;
    public ImageView imgLike;
    public View itemView;

    public ComentariosViewHolder(View itemView) {
        super(itemView);

        txtUsuario = (TextView) itemView.findViewById(R.id.txtEmail);
        txtComentario = (TextView) itemView.findViewById(R.id.txtComentario);
        txtLike = (TextView) itemView.findViewById(R.id.txtLike);
        txtTitulo = (TextView) itemView.findViewById(R.id.txtTitulo);
        imgAvatar = (ImageView)itemView.findViewById(R.id.imgAvatar);
        imgLike = (ImageView)itemView.findViewById(R.id.imgLike);
        this.itemView = itemView;
    }

    public void enviarComentario(final Comentario comentario, final Context context, View.OnClickListener listener, View.OnClickListener listenerLike) {
        txtUsuario.setText(comentario.getUsuario());
        txtComentario.setText(comentario.getMensaje());
        txtLike.setText(comentario.getLikes());
        txtTitulo.setText(comentario.getTitulo());
        if(comentario.getUrlPerfil().length()>0) {
            Picasso.with(context).load(comentario.getUrlPerfil()).transform(new CircleTransform()).into(imgAvatar);
        }else {
            Picasso.with(context).load(R.drawable.profile_msn).transform(new CircleTransform()).into(imgAvatar);
        }

        itemView.setOnClickListener(listener);
        imgLike.setOnClickListener(listenerLike);


    }
}
