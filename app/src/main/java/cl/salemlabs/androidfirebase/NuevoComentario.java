package cl.salemlabs.androidfirebase;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;

import java.util.HashMap;
import java.util.Map;

import cl.salemlabs.androidfirebase.entidades.Comentario;

/**
 * Created by harttynarce on 05-10-16.
 */

public class NuevoComentario {

    private static NuevoComentario instance;
    private OnClickComentarListener onClickComentarListener;

    private NuevoComentario() {
    }

    public static NuevoComentario getInstance() {
        if (instance == null) {
            instance = new NuevoComentario();
        }
        return instance;
    }

    public void alertComentario(final Context context, final String usuario, final OnClickComentarListener onClickComentarListener) {

        this.onClickComentarListener = onClickComentarListener;

        final Dialog dialog = new Dialog(context, R.style.FullHeightDialog);
        dialog.setContentView(R.layout.nuevo_comentario_dialog);

        ((TextView) dialog.findViewById(R.id.txtUsuario)).setText(usuario);
        ((TextView) dialog.findViewById(R.id.txtTitulo)).setText(R.string.comentar);
        final EditText txtComentario = (EditText) dialog.findViewById(R.id.txtComentario);
        final ImageView btnVolver = (ImageView) dialog.findViewById(R.id.btnVolver);
        final TextView btnEnviar = (TextView) dialog.findViewById(R.id.btnEnviar);


        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtComentario.getText().toString().trim().length() > 0) {
                    if (onClickComentarListener != null) {
                        onClickComentarListener.onClickComentar(usuario, txtComentario.getText().toString().trim());

                    }

                    dialog.dismiss();
                } else {
                    Toast.makeText(context, "Ingrese un comentario", Toast.LENGTH_SHORT).show();
                }
            }
        });


        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = context.getResources().getDisplayMetrics().widthPixels - (int) Utils.convertDpToPixel(20f, context);
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }


    interface OnClickComentarListener {
        void onClickComentar(String tutulo, String comentario);
    }

}
