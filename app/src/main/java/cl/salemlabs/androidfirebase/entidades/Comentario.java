package cl.salemlabs.androidfirebase.entidades;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by harttynandres on 03-10-16.
 */

public class Comentario {

    private String usuario;
    private String titulo;
    private String mensaje;
    private String uid;
    private String likes;
    private String urlPerfil;
    private Map<String, String> detalleLikes = new HashMap<>();

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Map<String, String> getDetalleLikes() {
        return detalleLikes;
    }

    public void setDetalleLikes(Map<String, String> detalleLikes) {
        this.detalleLikes = detalleLikes;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getUrlPerfil() {
        return urlPerfil;
    }

    public void setUrlPerfil(String urlPerfil) {
        this.urlPerfil = urlPerfil;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("usuario", usuario);
        result.put("mensaje", mensaje);
        result.put("detalleLikes", detalleLikes);
        result.put("titulo", titulo);
        result.put("likes", likes);
        result.put("urlPerfil", urlPerfil);
        result.put("uid", uid);
        return result;
    }
}
